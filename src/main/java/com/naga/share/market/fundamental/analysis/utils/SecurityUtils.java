package com.naga.share.market.fundamental.analysis.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Nagaaswin S
 *
 */
public final class SecurityUtils {

	private SecurityUtils() {
	}

	public static String passwordEncoder(String password) {
		return new BCryptPasswordEncoder().encode(password);
	}

	public static LocalDateTime setDateExpiration() {
		return LocalDateTime.now().plusYears(1);
	}

	public static LocalTime setTimeExpiration() {
		return LocalTime.now().plusMinutes(5);
	}

	public static LocalDate DOBDateConversion(String date) {
		String[] var = date.split("[-]");
		return LocalDate.of(Integer.parseInt(var[0]), Integer.parseInt(var[1]), Integer.parseInt(var[2]));
	}

	public static boolean expirationCheck(LocalDateTime dateTime) {
		return LocalDateTime.now().isAfter(dateTime);
	}

	public static boolean expirationCheck(LocalTime time) {
		return LocalTime.now().isAfter(time);
	}
}
