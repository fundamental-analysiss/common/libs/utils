package com.naga.share.market.fundamental.analysis.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.naga.share.market.fundamental.analysis.exception.InvalidDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.utils.constants.UtilityConstants;

/**
 * @author Nagaaswin S
 *
 */
public final class ValidationUtils {

	private static Logger logger = LogManager.getLogger(ValidationUtils.class);

	private ValidationUtils() {
	}

	/**
	 * A utility method to give environment value for a key value
	 * 
	 * @param envName
	 * @return
	 */
	public static String getEnvironment(String envName) {
		return System.getenv(envName);
	}

	/**
	 * A utility method to give current system time and date in dd/MM/yyyy HH:mm:ss
	 * pattern
	 * 
	 * @return
	 */
	public static String getSysDateTime() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
	}

	/**
	 * A utility method to check whether a object is not null
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isNotNull(Object obj) {
		if (null != obj) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a string is not empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		if (!str.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a List is not empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(List<?> variable) {
		if (!variable.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a array of objects is not empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(Object[] variable) {
		if (variable.length > 0) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a Map of objects is not empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(Map<?, ?> variable) {
		if (variable.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a string is not null and empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotNullAndEmpty(String str) {
		if (null != str && !str.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a list is not null and empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotNullAndEmpty(List<?> variable) {
		if (null != variable && !variable.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a array is not null and empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotNullAndEmpty(Object[] variable) {
		if (null != variable && variable.length > 0) {
			return true;
		}
		return false;
	}

	/**
	 * A utility method to check whether a array is not null and empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotNullAndEmpty(Map<?, ?> variable) {
		if (null != variable && variable.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * A Utility method to check the string data's are valid
	 * 
	 * @param data
	 */
	public static void isValidData(String... data) throws InvalidDataException {
		for (String str : data) {
			logger.debug("Entry isValidData Mehtod for data: {}", str);
			if (!isNotNullAndEmpty(str)) {
				logger.error(UtilityConstants.NULL_VALUE_ERROR);
				throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
			}
		}
	}

	/**
	 * A Utility method to check the object data's are valid
	 * 
	 * @param data
	 * @throws Exception
	 */
	public static void isValidData(Object... data) throws InvalidDataException {
		for (Object obj : data) {
			logger.debug("Entry isValidData Mehtod for data: {}", obj);
			if (!isNotNull(obj)) {
				logger.error(UtilityConstants.NULL_VALUE_ERROR);
				throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
			}
		}
	}

	public static void validateField(String input, String regEx, int minLength, int maxLength)
			throws InvalidDataException {
		logger.debug("Min Length: {},Max Length:{}", minLength, maxLength);
		Pattern pattern = Pattern.compile(regEx);
		if (isNotNullAndEmpty(input)) {
			boolean isMatched = pattern.matcher(input).matches();
			if (!isMatched || input.length() < minLength || input.length() > maxLength) {
				logger.error(UtilityConstants.SPEC_DOESNT_MATCH_ERROR);
				throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
			}
		} else {
			logger.error(UtilityConstants.NULL_VALUE_ERROR);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
		}
	}

	public static void validateField(int input, int minLength, int maxLength) throws InvalidDataException {
		logger.debug("Min Length: {},Max Length:{}", minLength, maxLength);
		if (isNotNull(input)) {
			if (String.valueOf(input).length() < minLength || String.valueOf(input).length() > maxLength) {
				logger.error(UtilityConstants.SPEC_DOESNT_MATCH_ERROR);
				throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
			}
		} else {
			logger.error(UtilityConstants.NULL_VALUE_ERROR);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
		}
	}
}
