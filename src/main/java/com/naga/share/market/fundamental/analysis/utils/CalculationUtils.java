package com.naga.share.market.fundamental.analysis.utils;

/**
 * @author Nagaaswin S
 *
 */
public final class CalculationUtils {

	private CalculationUtils() {

	}

	public static double margin(double value1, double value2) {
		return (value1 / value2) * 100;
	}

	public static double average(double... values) {
		double sum = 0.0;
		for (double value : values) {
			sum += value;
		}
		return (sum / values.length);
	}

	public static int average(int... values) {
		int sum = 0;
		for (int value : values) {
			sum += value;
		}
		return (sum / values.length);
	}

	public static double ratio(double value1, double value2) {
		return value1 / value2;
	}

	public static double ratio(int value1, int value2) {
		return value1 / value2;
	}
}
