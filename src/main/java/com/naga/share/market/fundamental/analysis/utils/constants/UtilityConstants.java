package com.naga.share.market.fundamental.analysis.utils.constants;

public class UtilityConstants {

	private UtilityConstants() {
	}

	public static final String NULL_VALUE_ERROR = "Null input value!!";
	public static final String SPEC_DOESNT_MATCH_ERROR = "Input does not satisfy specifications.";
}
